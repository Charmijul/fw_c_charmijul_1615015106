<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class buku_penulis extends Model
{
   protected $table = 'buku_penulis';
   protected $fillable = ['penulis_id','buku_id'];
}
