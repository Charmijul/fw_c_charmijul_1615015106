<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\buku;

class penulis extends Model
{
    protected $table = 'penulis';
    protected $fillable = ['nama','notlp','email','alamat'];

    public function buku(){
    	return $this->belongsToMany(buku::class);
    }
}
