<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class buku_pembeli extends Model
{
   protected $table = 'buku_pembeli';
   protected $fillable = ['pembeli_id','buku_id'];
}
