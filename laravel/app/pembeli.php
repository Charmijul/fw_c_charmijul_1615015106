<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\pengguna;

class pembeli extends Model
{
    protected $table = 'pembeli';
    protected $fillable = ['nama','notlp','email','alamat'];


    public function pengguna(){
    	return $this->belongsTo(pengguna::class);
    }

}
