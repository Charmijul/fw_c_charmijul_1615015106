@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-info">
	<div class="panel-heading">
		Data Admin
		<div class="pull-right">
			Tambah Data <a href="{{ url('admin/tambah')}}" class="btn btn-primary">Tambah</a>
		</div>
	</div>
	<div class="panel-body">
		<table class="table">
				<tr>
					<td>Nama</td>
					<td>No Telepon</td>
					<td>Email</td>
					<td>Alamat</td>
					<td>Pengguna ID</td>
				</tr>
				@foreach($admin as $Admin)
					
				<tr>
					<td >{{ $Admin->nama }}</td>
					<td >{{ $Admin->notlp}}</td>
					<td >{{ $Admin->email }}</td>
					<td >{{ $Admin->alamat}}</td>
					<td >{{ $Admin->pengguna_id}}</td>
					<td >
					
					<a href="{{url('admin/edit/'.$Admin->id)}}" class="btn btn-success btn-xs">Edit</a>
					<a href="{{url('admin/hapus/'.$Admin->id)}}" class="btn btn-danger btn-xs">Hapus</a>
					</td>
				</tr>
				@endforeach
			</table>
	</div>
</div>
@endsection
