<div class="form-group">  
	@if ($errors->any())
	<div class="alert alert-danger">
		<ul>             
			@foreach ($errors->all() as $error)                 
				<li>{{ $error }}</li>             
			@endforeach         
		</ul>  
	</div>
	@endif           
</div>
<div class="form-group">  
	<label class="col-sm-2">Judul</label>
	<div class="col-sm-9">
		{!! Form::text('judul',null,['class'=>'form-control','placeholder'=>"Judul"]) !!}  
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2">Penulis</label>    
	<div class="col-sm-9">   
		{!! Form::Label('penulis', 'Pilih Penulis') !!}   
		{!! Form::select('penulis', $author, null, ['class' => 'form-control']) !!} 
 	</div>
 </div>
 <div class="form-group">    
 	<label class="col-sm-2">Kategori</label>   
 	<div class="col-sm-9">
 		{!! Form::Label('kategori', 'Pilih Kategori') !!}
 		{!! Form::select('kategori', $categories, null, ['class' => 'form-control']) !!} 
 	</div>
 </div>
 <div class="form-group"> 
 	<label class="col-sm-2">Penerbit</label>   
 	<div class="col-sm-9">   
 		<!-- {!! Form::Label('penerbit', 'Penerbit') !!}   --> 
 		{!! Form::text('penerbit',null,['class'=>'form-control','placeholder'=>"Penerbit"]) !!}  
 	</div>
 </div> 
  <div class="form-group">
  	<label class="col-sm-2">Tanggal Rilis</label>   
 <div class="col-sm-9">   
 	<!-- {!! Form::Label('tanggal', 'Tanggal Rilis') !!} -->   
 	{!! Form::text('tanggal',null,['class'=>'form-control','placeholder'=>"Tanggal Rilis"]) !!}  
 </div>     
</div> 