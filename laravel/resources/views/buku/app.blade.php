@extends('master') 
@section('content')  
{{ $status or ' ' }}  
<div class="panel panel-info">   
	<div class="panel-heading">    
		<strong>Data Buku</strong>    
		<div class="pull-right">     
			Tambah Data <a href="{{url('buku/tambah')}}"><button class="btn btn-primary">Tambah</button></a>    
		</div>
	</div>    
	<div class="penel-body">     
		<table class="table">            
				<tr>       
					<td> Judul  </td>       
					<td> Kategori </td>       
					<td> Penerbit </td>       
					<td> Penulis </td>            
				</tr>      
				@foreach($datac as $buku) 

				<tr>   
					<td>{{ $buku->judul }}</td>   
					<td>{{ $buku->kategori->deskripsi or 'kosong'}}</td>   
					<td>{{ $buku->penerbit }}</td>   
					<td>{{ $buku->penulis->first()->nama or 'kosong'}}</td>   
					<td>    
						<a href="{{url('buku/edit/'.$buku->id)}}" class="btn btn-success btn-xs">edit</a>
						<a href="{{url('buku/hapus/'.$buku->id)}}" class="btn btn-danger btn-xs">delete</a>   
					</td>  
				</tr>      
			@endforeach     
		</table>    
	</div>   
</div>  
</div>                    
@endsection