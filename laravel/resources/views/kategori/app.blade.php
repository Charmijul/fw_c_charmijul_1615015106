@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-info">
	<div class="panel-heading">
		Data Kategori
		<div class="pull-right">
			Tambah Data <a href="{{ url('kategori/tambah')}}" class="btn btn-primary">Tambah</a>
		</div>
	</div>
	<div class="panel-body">
		<table class="table">
				
				<tr>
					<td>ID</td>
					<td>deskripsi</td>
					
				</tr>
				@foreach($kategori as $Kategori)
					
				<tr><td >{{ $Kategori->id }}</td>
					<td >{{ $Kategori->deskripsi }}</td>
					
					<td >
					
					<a href="{{url('kategori/edit/'.$Kategori->id)}}" class="btn btn-success btn-xs">edit</a>
					<a href="{{url('kategori/hapus/'.$Kategori->id)}}" class="btn btn-danger btn-xs">delete</a>
					</td>
				</tr>
				@endforeach
			</table>
	</div>
</div>
@endsection
