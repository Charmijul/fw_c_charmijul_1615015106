<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTableBukuPenulis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku_penulis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('penulis_id')->unsigned(); // foreign key
            $table->foreign('penulis_id')->references('id')
                                            ->on('penulis');
            $table->integer('buku_id')->unsigned(); // foreign key
            $table->foreign('buku_id')->references('id')
                                            ->on('buku');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku_penulis');
    }
}