<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});

Route::get('/home','kiky@test');

Route::get('/cobamodel',function(){

	$anggota =App\Anggota::all()->first();
	echo $anggota->nama;
	echo $anggota->alamat;
});

Route::get('/cobamodel3',function(){

	$anggota = new App\Anggota ;
	$anggota->nama='Wiwi';
	$anggota->alamat='Jalan SMP 10';
	$anggota->save();
});

Route::get('/tambah','penggunaController@tambah');

Route::get('/tambahpembeli','latihan_controller@create');

Route::get('/halamantambah', function () {
  return view('halaman_tambah');
});


Route::get('/relasi',function(){

	$anggota= app/anggota::where('nama', '=','Wiwi')
->first();
echo $anggota->nama .' '.'hobinya :';

foreach ($anggota->hobi as $list)
	echo '<li>'. $list->hobi;

});
*/

//-----ROUTES UNTUK TOKO BUKU (PRAKTIKUM)-----

Route::get('/tambah/penulis','penulisController@tambah');
Route::post('/penulis/simpan','penulisController@simpan');
Route::get('/penulis','penulisController@awal');
Route::get('/penulis/edit/{penulis}','penulisController@edit');
Route::post('/penulis/update/{penulis}','penulisController@update');
Route::get('/penulis/hapus/{penulis}','penulisController@hapus');

Route::get('/kategori',"KategoriController@awal");
Route::get('/kategori/tambah','kategoricontroller@tambah');
Route::post('/kategori/simpan','kategoricontroller@simpan');
Route::get('/kategori/edit/{kategori}','kategoricontroller@edit');
Route::post('/kategori/update/{kategori}','kategoricontroller@update');
Route::get('/kategori/hapus/{kategori}','kategoricontroller@hapus');

Route::get('/pembeli/tambah','pembelicontroller@tambah');
Route::post('/pembeli/simpan','pembelicontroller@simpan');
Route::get('/pembeli/edit/{pembeli}','pembelicontroller@edit');
Route::get('/pembeli','pembelicontroller@awal');
Route::post('pembeli/update/{pembeli}','pembelicontroller@update');
Route::get('pembeli/hapus/{pembeli}','pembelicontroller@hapus');

Route::get('/admin','admincontroller@awal');
Route::get('/admin/tambah',"admincontroller@tambah");
Route::post('/admin/simpan','admincontroller@simpan');
Route::get('/admin/lihat',"admincontroller@tambah");
Route::get('/admin/edit/{admin}','admincontroller@edit');
Route::post('admin/update/{admin}','admincontroller@update');
Route::get('admin/hapus/{admin}','admincontroller@hapus');
Route::get('admin/lihat','admincontroller@lihat');

Route::get('/buku','BukuController@awal');
Route::get('/buku/tambah',"bukuController@tambah");
Route::post('/buku/simpan','BukuController@simpan');
Route::get('/buku/lihat',"BukuController@tambah");
Route::get('buku/edit/{buku}','BukuController@edit');
Route::post('buku/update/{buku}','BukuController@update');
Route::get('buku/hapus/{buku}','BukuController@hapus');
Route::get('buku/lihat','BukuController@lihat');